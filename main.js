document.addEventListener('DOMContentLoaded', function () {
    var app = new Vue({
        el: '#app',
        data: {
            pagina: 'form',
            dia: '',
            platos: [],
            optionsDias: ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'],
            optionHorario: ['Mañana', 'Noche'],
            diaError: false,
            diaErrorMensaje: '*Debes selecionar el dia en el que quieres añadir la ficha',
            horario: '',
            horarioError: false,
            horarioErrorMensaje: '*Debes seleccionar el horario en el que quieres añadir la ficha',
            nombre: '',
            nombreError: false,
            NombreErrorMensaje: '*Debes poner un nombre a la comida que quieres añadir',
            ingredienteError: false,
            ingredienteMensajeError: '*Debe añadir al menos un ingrediente',
            nuevoIngrediente: '',
            ingredientes: [],
            loading: true,
        },
        mounted() {
            // Esperamos a que acabe de cargarse VUE para que se muestre la pagina, mientras mostrmamos un loading
            this.loading = false;
        },
        computed: {
            listaCompra: function () {
                let tempListaCompra = [];
                for (item of this.platos) {
                    tempListaCompra = _.concat(item.ingredientes, tempListaCompra);
                }
                return tempListaCompra;
            }
        },
        methods: {
            anyadirIngrediente: function() {
                // Nos aseguramos que no se puedan añadir listas vacias
                if (this.nuevoIngrediente != '' && this.nuevoIngrediente != ' ') {
                    // Subimos el value del input al array
                    this.ingredientes.push(this.nuevoIngrediente);
                    // Limpiamos el value del input
                    this.nuevoIngrediente = '';
                }
                this.validarIngredientes();
            },
            validarDia: function () {
                if (this.dia == '') {
                    this.diaError = true;
                } else {
                    this.diaError = false;
                }
            },
            validarHorario: function () {
                if (this.horario == '') {
                    this.horarioError = true;
                } else {
                    this.horarioError = false;
                }
            },
            validarNombre: function () {
                if (this.nombre == '') {
                    this.nombreError = true;
                } else {
                    this.nombreError = false;
                }
            },
            validarIngredientes: function () {
                if (this.ingredientes == '') {
                    this.ingredienteError = true;
                    this.$refs.inputIngrediente.focus();
                } else {
                    this.ingredienteError = false;
                }
            },
            anyadirPlato: function () {
                if(!this.diaError && !this.horarioError && !this.nombreError && !this.ingredienteError) {
                    this.platos.push({
                        dia: this.dia,
                        horario: this.horario,
                        nombre: this.nombre,
                        ingredientes: this.ingredientes,
                    });
                    this.ingredientes = [];
                    this.dia = '';
                    this.horario = '';
                    this.nombre = '';
                    this.pagina = 'tabla';
                } else {
                    this.validarFormulario();
                }
            },
            validarFormulario: function () {
                // Comprobamos que todo sea False
                this.validarDia();
                this.validarHorario();
                this.validarNombre();
                this.validarIngredientes();
            },
        }
    })
});
